package finder

import (
	"reflect"
	"sort"
	"testing"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

func TestFinder(t *testing.T) {
	t.Run("FindProjects", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			finder := Finder{}
			_, err := finder.FindProjects("empty")
			if err == nil {
				t.Fatal("Expected error but got none")
			}
		})

		var tcs = []struct {
			name   string
			dir    string
			config config
			want   []string
		}{

			// gemnasium
			{
				"gemnasium",
				"testdata/gemnasium",
				config{
					Preset:           PresetGemnasium,
					IgnoreHiddenDirs: true,
				},
				[]string{
					"bundler/gems.locked",
					"nested/bundler/Gemfile.lock",
					"nested/yarn/yarn.lock",
					"yarn/bundler/Gemfile.lock",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with max depth of 2
			{
				"gemnasium/max-depth/2",
				"testdata/gemnasium",
				config{
					Preset:   PresetGemnasium,
					MaxDepth: NullInt{true, 2},
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
					"nested/bundler/Gemfile.lock",
					"nested/yarn/yarn.lock",
					"yarn/bundler/Gemfile.lock",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with max depth of 1
			{
				"gemnasium/max-depth/1",
				"testdata/gemnasium",
				config{
					Preset:   PresetGemnasium,
					MaxDepth: NullInt{true, 1},
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with max depth of 0
			{
				"gemnasium/max-depth/0",
				"testdata/gemnasium/yarn",
				config{
					Preset:   PresetGemnasium,
					MaxDepth: NullInt{true, 0},
				},
				[]string{
					"yarn.lock",
				},
			},

			// gemnasium with ignored dirs
			{
				"gemnasium/ignored-dirs",
				"testdata/gemnasium",
				config{
					Preset:           PresetGemnasium,
					IgnoredDirs:      []string{"nested", "yarn"},
					IgnoreHiddenDirs: false,
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
				},
			},

			// gemnasium-maven
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset: PresetGemnasiumMaven,
				},
				[]string{
					"app3/pom.xml",
					"app3/api/pom.xml",
					"app3/api/api1/pom.xml",
					"app3/api/api2/pom.xml",
					"app3/model/pom.xml",
					"app3/web/pom.xml",
				},
			},

			// gemnasium with max depth of 1
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset:   PresetGemnasiumMaven,
					MaxDepth: NullInt{true, 1},
				},
				[]string{
					"app3/pom.xml",
				},
			},

			// gemnasium with max depth of 3
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset:   PresetGemnasiumMaven,
					MaxDepth: NullInt{true, 3},
				},
				[]string{
					"app3/pom.xml",
					"app3/api/pom.xml",
					"app3/api/api1/pom.xml",
					"app3/api/api2/pom.xml",
					"app3/model/pom.xml",
					"app3/web/pom.xml",
				},
			},

			// gemnasium-python
			{
				"gemnasium-python",
				"testdata/gemnasium-python",
				config{
					Preset: PresetGemnasiumPython,
				},
				[]string{
					"app/requirements.txt",
				},
			},

			// gemnasium-python with pip requirements filename
			{
				"gemnasium-python/pip-requirements-file",
				"testdata/gemnasium-python/app/server",
				config{
					Preset:              PresetGemnasiumPython,
					PipRequirementsFile: "requirements-production.txt",
				},
				[]string{
					"requirements-production.txt",
					"requirements.txt",
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				// find projects
				finder := newFinder(tc.config)
				projects, err := finder.FindProjects(tc.dir)
				if err != nil {
					t.Fatal(err)
				}

				// extract, sort file paths
				got := []string{}
				for _, project := range projects {
					for _, file := range project.Files {
						got = append(got, project.FilePath(file))
					}
				}
				sort.Strings(got)
				sort.Strings(tc.want)

				// compare sorted paths
				if !reflect.DeepEqual(tc.want, got) {
					t.Errorf("Wrong result. Expecting:\n%+v\nbut got:\n%+v", tc.want, got)
				}
			})
		}
	})
}
