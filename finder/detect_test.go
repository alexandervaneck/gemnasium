package finder

import (
	"reflect"
	"sort"
	"testing"
)

func TestDetect(t *testing.T) {
	var tcs = []struct {
		name        string
		pkgManagers []PackageManager
		filenames   []string
		want        []Project
	}{

		// detect bundler project
		// ignore yarn project (package manager not enabled)
		{
			"bundler",
			[]PackageManager{
				PackageManagerBundler,
			},
			[]string{
				"Gemfile",
				"Gemfile.lock",
				"package.json", // ignored
				"yarn.lock",    // ignored
			},
			[]Project{
				{
					Files: []File{
						{Filename: "Gemfile", FileType: FileTypeRequirements},
						{Filename: "Gemfile.lock", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerBundler,
				},
			},
		},

		// detect bundler project with alternative file names
		// ignore yarn project (package manager not enabled)
		{
			"bundler2",
			[]PackageManager{
				PackageManagerBundler,
			},
			[]string{
				"gems.rb",
				"package.json", // ignored
				"yarn.lock",    // ignored
				"gems.locked",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "gems.rb", FileType: FileTypeRequirements},
						{Filename: "gems.locked", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerBundler,
				},
			},
		},

		// detect bundler project having no lock file
		{
			"bundler3",
			[]PackageManager{
				PackageManagerBundler,
			},
			[]string{
				"Gemfile",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "Gemfile", FileType: FileTypeRequirements},
					},
					PackageManager: PackageManagerBundler,
				},
			},
		},

		// detect nuget project
		// detect requirements file api.csproj based on its extension
		// ignore yarn project (package manager not enabled)
		{
			"nuget",
			[]PackageManager{
				PackageManagerNuget,
			},
			[]string{
				"package.json", // ignored
				"yarn.lock",    // ignored
				"api.csproj",
				"packages.lock.json",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "api.csproj", FileType: FileTypeRequirements},
						{Filename: "packages.lock.json", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerNuget,
				},
			},
		},

		// detect yarn project based on lock file; npm is rejected
		{
			"yarn",
			[]PackageManager{
				PackageManagerNpm,
				PackageManagerYarn,
			},
			[]string{
				"package.json",
				"yarn.lock",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "package.json", FileType: FileTypeRequirements},
						{Filename: "yarn.lock", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerYarn,
				},
			},
		},

		// detect npm project based on lock file; yarn is rejected
		{
			"npm",
			[]PackageManager{
				PackageManagerNpm,
				PackageManagerYarn,
			},
			[]string{
				"package.json",
				"package-lock.json",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "package.json", FileType: FileTypeRequirements},
						{Filename: "package-lock.json", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerNpm,
				},
			},
		},

		// detect yarn project and bundler project
		{
			"bundler+yarn",
			[]PackageManager{
				PackageManagerBundler,
				PackageManagerYarn,
			},
			[]string{
				"package.json",
				"yarn.lock",
				"Gemfile",
				"Gemfile.lock",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "Gemfile", FileType: FileTypeRequirements},
						{Filename: "Gemfile.lock", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerBundler,
				},
				{
					Files: []File{
						{Filename: "package.json", FileType: FileTypeRequirements},
						{Filename: "yarn.lock", FileType: FileTypeLockFile},
					},
					PackageManager: PackageManagerYarn,
				},
			},
		},

		// keep 1 project with "maven" type
		// pick first package manager
		{
			"maven+gradle+sbt",
			[]PackageManager{
				PackageManagerMaven,
				PackageManagerGradle,
				PackageManagerSbt,
			},
			[]string{
				"pom.xml",
				"build.gradle",
				"build.sbt",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "pom.xml", FileType: FileTypeRequirements},
					},
					PackageManager: PackageManagerMaven,
				},
			},
		},

		// keep 1 project with "maven" type
		// pick first package manager
		{
			"gradle+sbt+maven",
			[]PackageManager{
				PackageManagerGradle,
				PackageManagerSbt,
				PackageManagerMaven,
			},
			[]string{
				"pom.xml",
				"build.gradle",
				"build.sbt",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "build.gradle", FileType: FileTypeRequirements},
					},
					PackageManager: PackageManagerGradle,
				},
			},
		},

		// keep 1 project with "pypi" type
		// pick first package manager
		{
			"pip+setuptools",
			[]PackageManager{
				PackageManagerPip,
				PackageManagerSetuptools,
			},
			[]string{
				"setup.py",
				"requirements.txt",
			},
			[]Project{
				{
					Files: []File{
						{Filename: "requirements.txt", FileType: FileTypeRequirements},
					},
					PackageManager: PackageManagerPip,
				},
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			detect := NewDetect(tc.pkgManagers...)
			got := detect(tc.filenames)

			// sort projects to make sure this test isn't flaky
			sort.Slice(got, func(i, j int) bool { return got[i].PackageManager.Name < got[j].PackageManager.Name })
			sort.Slice(tc.want, func(i, j int) bool { return got[i].PackageManager.Name < got[j].PackageManager.Name })

			if !reflect.DeepEqual(got, tc.want) {
				t.Errorf("Wrong result. Expected:\n%#v\nGot:\n%#v)", tc.want, got)
			}
		})
	}
}
