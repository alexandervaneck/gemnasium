package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "gemnasium",
		Name:    "Gemnasium",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
