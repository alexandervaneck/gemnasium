package cli

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

// Register registers a CLI-based resolver.
func Register(syntaxName, path string, args ...string) {
	r := &Resolver{
		Name: syntaxName,
		Path: path,
		Args: args,
	}
	vrange.Register(syntaxName, r)
}

// Resolver is a CLI-based resolver.
type Resolver struct {
	Name string   // Name is the name used to configure the resolver.
	Path string   // Path is the path of the command to run.
	Args []string // Args are the command arguments.
}

// Flags returns the CLI flags that configure the resolver.
func (r Resolver) Flags() []cli.Flag {
	usage := fmt.Sprintf("Path to CLI command that evaluates version range for %s", r.Name)
	envVar := fmt.Sprintf("VRANGE_%s_CMD", strings.ToUpper(r.Name))
	value := r.Path
	if len(r.Args) != 0 {
		value += " " + strings.Join(r.Args, " ")
	}

	return []cli.Flag{
		&cli.StringFlag{
			Name:    r.cmdFlag(),
			Usage:   usage,
			EnvVars: []string{envVar},
			Value:   value,
		},
	}
}

// cmdFlag returns the name of the CLI flag that configures the CLI.
func (r Resolver) cmdFlag() string {
	return fmt.Sprintf("vrange-%s-cmd", strings.ToLower(r.Name))
}

// Configure configures the CLI-based resolver with CLI context and options,
// and ensures that the command exists.
func (r *Resolver) Configure(c *cli.Context, opts vrange.Options) error {
	cmd := c.String(r.cmdFlag())
	slice := strings.SplitN(cmd, " ", 2)
	path := filepath.Join(opts.BaseDir, slice[0])
	if _, err := os.Stat(path); err != nil {
		return err
	}
	r.Path = path
	r.Args = slice[1:]
	return nil
}

// Resolve evaluates queries.
func (r Resolver) Resolve(queries []vrange.Query) (*vrange.ResultSet, error) {

	// create input document
	tmpfile, err := ioutil.TempFile("/tmp", "vrange_queries")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmpfile.Name())
	if err := json.NewEncoder(tmpfile).Encode(queries); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	// run command, read output
	args := append(r.Args, tmpfile.Name())
	cmd := exec.Command(r.Path, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	// decode results
	results := []struct {
		Version   string
		Range     string
		Satisfies bool
		Error     string
	}{}
	if err := json.Unmarshal(out, &results); err != nil {
		return nil, err
	}

	// build result set
	set := make(vrange.ResultSet)
	for _, result := range results {
		var err error = nil
		if result.Error != "" {
			err = errors.New(result.Error)
		}
		query := vrange.Query{Version: result.Version, Range: result.Range}
		set.Set(query, result.Satisfies, err)
	}
	return &set, nil
}
