package vrange

import "strings"

// ErrResolverNotFound is a resolver error
type ErrResolverNotFound struct {
	Name           string
	AvailableNames []string
}

func (err ErrResolverNotFound) Error() string {
	return "resolver not found: " + err.Name +
		"; available resolvers: " + strings.Join(err.AvailableNames, " ")
}

// ErrQueryNotFound is a query error
type ErrQueryNotFound struct {
	Query Query
}

func (err ErrQueryNotFound) Error() string {
	return "no result for query: " + err.Query.String()
}
