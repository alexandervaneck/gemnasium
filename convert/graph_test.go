package convert

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestGraph(t *testing.T) {
	pkgs := []parser.Package{
		{"a", "1"},     // 1
		{"b", "1.1"},   // 2
		{"c", "1.2"},   // 3
		{"d", "1.2.1"}, // 4
		{"e", "2"},     // 5
		{"f", "2.1"},   // 6
	}

	deps := []parser.Dependency{
		{
			To: &parser.Package{"a", "1"},
		},
		{
			From: &parser.Package{"a", "1"},
			To:   &parser.Package{"b", "1.1"},
		},
		{
			From: &parser.Package{"a", "1"},
			To:   &parser.Package{"c", "1.2"},
		},
		{
			From: &parser.Package{"c", "1.2"},
			To:   &parser.Package{"d", "1.2.1"},
		},
		{
			To: &parser.Package{"e", "2"},
		},
		{
			From: &parser.Package{"e", "2"},
			To:   &parser.Package{"f", "2.1"},
		},
		{
			From: &parser.Package{"f", "2.1"},
			To:   &parser.Package{"d", "1.2.1"},
		},
	}

	index := NewIndex(pkgs)
	graph := NewGraph(pkgs, deps, index)

	var tcs = []struct {
		name  string
		pkg   parser.Package
		paths [][]Node // possible paths
	}{
		{
			"direct",
			parser.Package{"a", "1"},
			[][]Node{{}},
		},
		{
			"direct 2",
			parser.Package{"e", "2"},
			[][]Node{{}},
		},
		{
			"2nd level",
			parser.Package{"b", "1.1"},
			[][]Node{
				{
					{1, &parser.Package{"a", "1"}},
				},
			},
		},
		{
			"3rd level",
			parser.Package{"d", "1.2.1"},
			[][]Node{
				{
					{1, &parser.Package{"a", "1"}},
					{3, &parser.Package{"c", "1.2"}},
				},
				{
					{5, &parser.Package{"e", "2"}},
					{6, &parser.Package{"f", "2.1"}},
				},
			},
		},
	}

	t.Run("PathTo", func(t *testing.T) {
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				got := graph.PathTo(tc.pkg)

				// iterate possible paths until there's a match
				for _, want := range tc.paths {
					if reflect.DeepEqual(want, got) {
						return // match
					}
				}
				t.Errorf("wrong result. expected one of %v but got %v", tc.paths, got)
			})
		}
	})
}
