package nuget

import (
	"encoding/json"
	"os"
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestNuget(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			// Load fixture
			fixture, err := os.Open("fixtures/wrong_version/packages.lock.json")
			if err != nil {
				t.Fatal("Can't open fixture file", err)
			}
			defer fixture.Close()
			_, _, err = Parse(fixture)
			if err != parser.ErrWrongFileFormatVersion {
				t.Fatalf(`Expected "%v" error but got "%v"`, parser.ErrWrongFileFormatVersion, err)
			}

		})

		for _, tc := range []string{"web.api", "duplicates"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/packages.lock.json")
				if err != nil {
					t.Fatal("Can't open fixture file", err)
				}
				defer fixture.Close()
				pkgs, deps, err := Parse(fixture)
				if err != nil {
					t.Fatal(err)
				}

				t.Run("packages", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/packages.json")
					if err != nil {
						t.Fatal("Can't open expect file", err)
					}
					defer expect.Close()
					var want []parser.Package
					err = json.NewDecoder(expect).Decode(&want)
					if err != nil {
						t.Fatal(err)
					}

					// Sort & Compare
					// TODO: replace with assert.ElementsMatch(t, got, want) when migrating to stretchr/testify
					got := pkgs
					sortPackages(got)
					sortPackages(want)
					if !reflect.DeepEqual(got, want) {
						t.Errorf("Wrong result. Expected\n%v\nbut got\n%v", want, got)
					}
				})

				t.Run("dependencies", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/dependencies.json")
					if err != nil {
						t.Fatal("Can't open expect file", err)
					}
					defer expect.Close()
					var want []parser.Dependency
					err = json.NewDecoder(expect).Decode(&want)
					if err != nil {
						t.Fatal(err)
					}

					assert.ElementsMatch(t, deps, want)
				})
			})
		}
	})
}

// TODO: remove this function when migrating to stretchr/testify
func sortPackages(deps []parser.Package) {
	sort.SliceStable(deps, func(i, j int) bool {
		if deps[i].Name != deps[j].Name {
			return deps[i].Name < deps[j].Name
		}
		return deps[i].Version < deps[j].Version
	})
}
