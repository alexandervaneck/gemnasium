package nuget

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Document is a NuGet lock file
type Document struct {
	// Version is the format version
	Version int

	// Dependencies maps the .NET build targets to their dependencies
	Dependencies map[string]TargetDependencies
}

// TargetDependencies are all the dependencies of a .NET build target.
// This includes both direct dependencies and transitive dependencies.
type TargetDependencies map[string]DependencyInfo

// DependencyInfo describes a NuGet dependency
type DependencyInfo struct {
	// Type tells whether this is a direct project dependency.
	// It is either "direct" or "transitive".
	Type string

	// Requested is the requested version range.
	// It is set only for direct dependencies.
	Requested string

	// Resolved is the resolved version
	Resolved string

	// Dependencies is a list of transitive dependencies. It maps a package name
	// to a requested version range if the dependent package is a direct project dependency
	// (type is direct), and an exact version otherwise (type is transitive).
	Dependencies map[string]string

	// ContentHash string `json:"contentHash"`
}

const supportedFileFormatVersion = 1

// Parse scans a NuGet lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	// decode lock file
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}

	// check format version
	if document.Version != supportedFileFormatVersion {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}

	/* iterate through targets, build a package list and a package map

	Duplicates with same name and version are ignored when adding packages to the list.
	this cover the case where the same package appears in different targets but with differnt versions.

	Duplicates with same name are ignored when adding to the map.
	This approximation is necessary because neither because neither the "requested" field
	or the "resolved" fields are reliable when resolving dependencies.

	TODO: investigate to see if a lock file might have multiple targets with different package versions
	*/
	pkgs := []parser.Package{}
	seenPkgs := map[parser.Package]bool{}
	pkgMap := map[string]*parser.Package{}
	for _, d := range document.Dependencies {
		for name, info := range d {
			// append package if not a duplicate
			pkg := parser.Package{Name: name, Version: info.Resolved}
			if _, ok := seenPkgs[pkg]; ok {
				continue
			}
			seenPkgs[pkg] = true
			pkgs = append(pkgs, pkg)

			// update dependency map
			pkgMap[name] = &pkg
		}
	}

	// iterate through targets, resolve dependencies, build a dependency list
	deps := []parser.Dependency{}
	for _, d := range document.Dependencies {
		for name, info := range d {
			// retrieve dependent package
			pkg, _ := pkgMap[name]

			// append direct dependency if "direct"
			if info.Type == "Direct" {
				deps = append(deps, parser.Dependency{
					To:           pkg,
					VersionRange: info.Requested,
				})
			}

			// append transitive dependencies
			for depName, depVersion := range info.Dependencies {
				// resolve dependency name to package
				toPkg, ok := pkgMap[depName]
				if !ok {
					return nil, nil, fmt.Errorf("cannot find nuget dependency: %s", depName)
				}

				// append dependency
				deps = append(deps, parser.Dependency{
					From:         pkg,
					To:           toPkg,
					VersionRange: depVersion,
				})
			}

		}
	}

	return pkgs, deps, nil
}

func init() {
	parser.Register("nuget", parser.Parser{
		Parse:          Parse,
		PackageManager: "nuget",
		PackageType:    parser.PackageTypeNuget,
		Filenames:      []string{"packages.lock.json"},
	})
}
