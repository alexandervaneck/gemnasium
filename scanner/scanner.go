package scanner

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	flagPrefix           = "gemnasium-"
	flagDBUpdateDisabled = flagPrefix + "db-update-disabled"
	flagDBLocalPath      = flagPrefix + "db-local-path"
	flagDBRemoteURL      = flagPrefix + "db-remote-url"
	flagDBWebURL         = flagPrefix + "db-web-url"
	flagDBRefName        = flagPrefix + "db-ref-name"

	envVarPrefix           = "GEMNASIUM_"
	envVarDBUpdateDisabled = envVarPrefix + "DB_UPDATE_DISABLED"
	envVarDBLocalPath      = envVarPrefix + "DB_LOCAL_PATH"
	envVarDBRemoteURL      = envVarPrefix + "DB_REMOTE_URL"
	envVarDBWebURL         = envVarPrefix + "DB_WEB_URL"
	envVarDBRefName        = envVarPrefix + "DB_REF_NAME"
)

// Flags generates new command line flags for the scanner
func Flags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagDBUpdateDisabled,
			EnvVars: []string{envVarDBUpdateDisabled},
			Usage:   "Disable gemnasium-db git repo update before scanning",
			Value:   false,
		},
		&cli.StringFlag{
			Name:    flagDBLocalPath,
			EnvVars: []string{envVarDBLocalPath},
			Usage:   "Path of gemnasium-db git repo",
			Value:   "gemnasium-db",
		},
		&cli.StringFlag{
			Name:    flagDBRemoteURL,
			EnvVars: []string{envVarDBRemoteURL},
			Usage:   "Remote URL the local gemnasium-db git repo is synced with",
		},
		&cli.StringFlag{
			Name:    flagDBWebURL,
			EnvVars: []string{envVarDBWebURL},
			Usage:   "Web URL of the gemnasium-db GitLab project",
		},
		&cli.StringFlag{
			Name:    flagDBRefName,
			EnvVars: []string{envVarDBRefName},
			Usage:   "git reference the local gemnasium-db git repo is synced with",
		},
	}
}

// NewScanner parses command line arguments from a cli.Context and returns a new scanner
func NewScanner(c *cli.Context) (*Scanner, error) {
	// GitLab project and git repo for the advisories
	r := advisory.Repo{
		Path:      c.String(flagDBLocalPath),
		RemoteURL: c.String(flagDBRemoteURL),
		WebURL:    c.String(flagDBWebURL),
		RefName:   c.String(flagDBRefName),
	}

	// update advisory repo if requested
	if !c.Bool(flagDBUpdateDisabled) {
		if err := r.Update(); err != nil {
			return nil, err
		}
	}

	return &Scanner{r}, nil
}

// Scanner is a project scanner
type Scanner struct {
	repo advisory.Repo
}

// ScanProjects scans projects and returns a description of each file it has scanned.
// This includes the dependencies found in the file, and the vulnerabilities affecting them.
func (s Scanner) ScanProjects(dir string, projects []finder.Project) ([]File, error) {
	result := []File{}
	for _, project := range projects {
		scannable, isScannable := project.ScannableFile()
		if !isScannable {
			// skip project
			log.Debugf("skip project: no scannable file found in %s", project.Dir)
			continue
		}

		// relative and absolute path of scannable file
		rel := project.FilePath(scannable)
		path := filepath.Join(dir, rel)

		// set location to relative path of scannable file
		location := rel
		if !scannable.Linkable() {
			// update location with the relative path of the requirements file
			if requirements, ok := project.RequirementsFile(); ok {
				location = project.FilePath(requirements)
			}
		}

		// scan scannable file
		scanned, err := s.ScanFile(path, location)
		if err != nil {
			return nil, err
		}
		scanned.RootDir = dir
		result = append(result, *scanned)
	}

	return result, nil
}

// ScanFile opens, parses, and scans a dependency file.
// It returns the dependencies listed in the file
// along with the vulnerabilities affecting them.
// The alias overrides the file path so that the location of a vulnerability
// or a dependency can be overridden by this parameter.
func (s Scanner) ScanFile(path, alias string) (*File, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fileType := filepath.Base(path)
	return s.scanReader(f, fileType, alias)
}

func (s Scanner) scanReader(reader io.Reader, fileType, filePath string) (*File, error) {
	depParser := parser.Lookup(fileType)
	if depParser == nil {
		return nil, fmt.Errorf("no parser for file type %s, path %s", fileType, filePath)
	}
	depfile, err := parseReader(reader, *depParser, filePath)
	if err != nil {
		return nil, err
	}
	aff, err := fileAffections(*depfile, s.repo)
	if err != nil {
		return nil, err
	}
	depfile.Affections = aff
	return depfile, nil
}

func parseReader(reader io.Reader, depParser parser.Parser, filePath string) (*File, error) {
	pkgs, deps, err := depParser.Parse(reader)
	if err != nil {
		return nil, err
	}

	return &File{
		Path:           filePath,
		PackageManager: depParser.PackageManager,
		PackageType:    string(depParser.PackageType),
		Packages:       pkgs,
		Dependencies:   deps,
	}, nil
}

type queryTranslator func(vrange.Query, []advisory.Version) vrange.Query

func fileAffections(depfile File, repo advisory.Repo) ([]Affection, error) {
	pkgType := depfile.PackageType

	// version range resolver
	resolver, err := vrange.NewResolver(pkgTypeToResolverName(pkgType))
	if err != nil {
		return nil, err
	}

	// query translator
	var translateQuery queryTranslator
	switch t := resolver.(type) {
	default:
		// use identify function
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return q
		}
	case vrange.QueryTranslator:
		// delegate to resolver
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return t.TranslateQuery(q, versionMeta)
		}
	}

	// ensure repo has advisories matching the package type
	if err := repo.SatisfyPackageType(pkgType); err != nil {
		return nil, err
	}

	// collect possible affections, list version range queries
	affections := []Affection{}
	queries := []vrange.Query{}
	for _, dep := range depfile.Packages {

		// list package advisories
		pkg := advisory.Package{Type: pkgType, Name: dep.Name}
		paths, err := repo.PackageAdvisories(pkg)
		if _, ok := err.(advisory.ErrNoPackageDir); ok {
			continue // no advisories
		}
		if err != nil {
			return nil, err
		}

		// fetch advisories, append affections and queries
		for _, path := range paths {
			adv, err := repo.Advisory(path)
			if err != nil {
				return nil, err
			}
			aff := Affection{dep, *adv}
			affections = append(affections, aff)
			q := translateQuery(aff.query(), aff.Advisory.Versions)
			queries = append(queries, q)

		}
	}

	// resolve queries and tell if dependency version is in affected range
	result, err := resolver.Resolve(queries)
	if err != nil {
		return nil, err
	}

	// filter affections and keep those where the dependency version is in affected range
	filtered := []Affection{}
	for _, aff := range affections {
		q := translateQuery(aff.query(), aff.Advisory.Versions)
		ok, err := result.Satisfies(q)
		if err != nil {
			// TODO report error
			continue
		}
		if ok {
			filtered = append(filtered, aff)
		}
	}
	return filtered, nil
}

// pkgTypeToResolverName converts a package type to the name of a vrange resolver.
func pkgTypeToResolverName(pkgType string) string {
	switch pkgType {
	case "npm", "maven", "gem", "go":
		return pkgType
	case "packagist":
		return "php"
	case "pypi":
		return "python"
	default:
		return pkgType
	}
}
