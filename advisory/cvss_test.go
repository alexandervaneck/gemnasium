package advisory

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestSeverity(t *testing.T) {
	testCases := []struct {
		name, cvss2, cvss3 string
		wantErr            error
		want               report.SeverityLevel
	}{
		{
			name:  "it returns the cvss3 score when both cvss2 and cvss3 vectors are present",
			cvss2: "(AV:N/AC:L/Au:S/C:P/I:P/A:N)",                 // score 5.5 - medium
			cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H", // score 7.5 - high
			want:  report.SeverityLevelHigh,
		},
		{
			name:  "it returns the cvss2 score when an error occurs while parsing the cvss3 vector",
			cvss2: "(AV:N/AC:L/Au:S/C:P/I:P/A:N)", // score 5.5 - medium
			cvss3: "bad-cvss3-vector",
			want:  report.SeverityLevelMedium,
		},
		{
			name:  "it returns the cvss2 score when the cvss3 vector is empty",
			cvss2: "(AV:A/AC:L/Au:S/C:N/I:N/A:P)", // score 2.7 - low
			cvss3: "",
			want:  report.SeverityLevelLow,
		},
		{
			name:  "it handles a cvss2 medium score",
			cvss2: "(AV:N/AC:L/Au:S/C:P/I:P/A:N)", // score 5.5 - medium
			cvss3: "",
			want:  report.SeverityLevelMedium,
		},
		{
			name:  "it handles a cvss2 high score",
			cvss2: "(AV:N/AC:L/Au:N/C:C/I:N/A:P)", // score 8.5 - high
			cvss3: "",
			want:  report.SeverityLevelHigh,
		},
		{
			name:  "it returns SeverityLevelUnknown when neither the cvss2 nor cvss3 vectors can be parsed",
			cvss2: "bad-cvss2-vector",
			cvss3: "bad-cvss3-vector",
			want:  report.SeverityLevelUnknown,
		},
		{
			name:  "it returns SeverityLevelUnknown when both the cvss2 and cvss3 vectors are empty",
			cvss2: "",
			cvss3: "",
			want:  report.SeverityLevelUnknown,
		},
		{
			name:  "it supports a cvss v3.1 vector",
			cvss2: "(AV:L/AC:M/Au:N/C:N/I:P/A:P)",                 // score 3.3 - low
			cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:L/I:L/A:N", // score  4.6 - medium
			want:  report.SeverityLevelMedium,
		},
		{
			name:  "it adds required parentheses to the cvss2 vector",
			cvss2: "AV:L/AC:M/Au:N/C:N/I:P/A:P", // score 3.3 - low
			cvss3: "",
			want:  report.SeverityLevelLow,
		},
		{
			name:  "it handles a cvss v3.1 critical vector",
			cvss2: "AV:L/AC:M/Au:N/C:N/I:P/A:P",                   // score 3.3 - low
			cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H", // score 9.9 - critical
			want:  report.SeverityLevelCritical,
		},
		{
			name:  "it handles a cvss v3.0 none vector",
			cvss2: "AV:L/AC:M/Au:N/C:N/I:P/A:P",                   // score 3.3 - low
			cvss3: "CVSS:3.0/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N", // score 0.0 - None
			want:  report.SeverityLevelInfo,
		},
		// taken from https://github.com/spiegel-im-spiegel/go-cvss/blob/v0.2.1/v3/base/base_test.go#L76-L120
		//CVSSv3.0
		{name: "score: 0.0         error", cvss2: "", cvss3: "CVSS:3.0/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:X", want: report.SeverityLevelUnknown},
		{name: "score: 0.0  Zero metrics", cvss2: "", cvss3: "CVSS:3.0/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N", want: report.SeverityLevelInfo},
		{name: "score: 7.5 CVE-2015-8252", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N", want: report.SeverityLevelHigh},
		{name: "score: 6.1 CVE-2013-1937", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 6.4 CVE-2013-0375", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 3.1 CVE-2014-3566", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:N", want: report.SeverityLevelLow},
		{name: "score: 9.9 CVE-2012-1516", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H", want: report.SeverityLevelCritical},
		{name: "score: 8.8 CVE-2012-0384", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.8 CVE-2015-1098", cvss2: "", cvss3: "CVSS:3.0/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.5 CVE-2014-0160", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N", want: report.SeverityLevelHigh},
		{name: "score: 9.8 CVE-2014-6271", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelCritical},
		{name: "score: 6.8 CVE-2008-1447", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:C/C:N/I:H/A:N", want: report.SeverityLevelMedium},
		{name: "score: 6.8 CVE-2014-2005", cvss2: "", cvss3: "CVSS:3.0/AV:P/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelMedium},
		{name: "score: 5.8 CVE-2010-0467", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N", want: report.SeverityLevelMedium},
		{name: "score: 5.8 CVE-2012-1342", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:N/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 5.4 CVE-2014-9253", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 7.8 CVE-2009-0658", cvss2: "", cvss3: "CVSS:3.0/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 8.8 CVE-2011-1265", cvss2: "", cvss3: "CVSS:3.0/AV:A/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 4.6 CVE-2014-2019", cvss2: "", cvss3: "CVSS:3.0/AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N", want: report.SeverityLevelMedium},
		{name: "score: 8.8 CVE-2015-0970", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.4 CVE-2014-0224", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N", want: report.SeverityLevelHigh},
		{name: "score: 9.6 CVE-2012-5376", cvss2: "", cvss3: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H", want: report.SeverityLevelCritical},
		//CVSSv3.1
		{name: "score: 0.0  Zero metrics", cvss2: "", cvss3: "CVSS:3.1/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N", want: report.SeverityLevelInfo},
		{name: "score: 7.5 CVE-2015-8252", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N", want: report.SeverityLevelHigh},
		{name: "score: 6.1 CVE-2013-1937", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 6.4 CVE-2013-0375", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 3.1 CVE-2014-3566", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:N", want: report.SeverityLevelLow},
		{name: "score: 9.9 CVE-2012-1516", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H", want: report.SeverityLevelCritical},
		{name: "score: 8.8 CVE-2012-0384", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.8 CVE-2015-1098", cvss2: "", cvss3: "CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.5 CVE-2014-0160", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N", want: report.SeverityLevelHigh},
		{name: "score: 9.8 CVE-2014-6271", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelCritical},
		{name: "score: 6.8 CVE-2008-1447", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:C/C:N/I:H/A:N", want: report.SeverityLevelMedium},
		{name: "score: 6.8 CVE-2014-2005", cvss2: "", cvss3: "CVSS:3.1/AV:P/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelMedium},
		{name: "score: 5.8 CVE-2010-0467", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N", want: report.SeverityLevelMedium},
		{name: "score: 5.8 CVE-2012-1342", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:N/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 5.4 CVE-2014-9253", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N", want: report.SeverityLevelMedium},
		{name: "score: 7.8 CVE-2009-0658", cvss2: "", cvss3: "CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 8.8 CVE-2011-1265", cvss2: "", cvss3: "CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 4.6 CVE-2014-2019", cvss2: "", cvss3: "CVSS:3.1/AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N", want: report.SeverityLevelMedium},
		{name: "score: 8.8 CVE-2015-0970", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", want: report.SeverityLevelHigh},
		{name: "score: 7.4 CVE-2014-0224", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N", want: report.SeverityLevelHigh},
		{name: "score: 9.6 CVE-2012-5376", cvss2: "", cvss3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H", want: report.SeverityLevelCritical},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			a := Advisory{CVSSV2: tc.cvss2, CVSSV3: tc.cvss3}
			got := a.Severity()
			if !reflect.DeepEqual(got, tc.want) {
				t.Errorf("Wrong result. Expected %#v but got %#v", tc.want, got)
			}
		})
	}
}
